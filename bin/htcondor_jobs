#!/usr/bin/env python
"""
Create and check the status of CMSSW jobs created for the HT-condor scheduler

Usage
-----
$ htcondor create PYCFG SCRIPTNAME [-o outputdir -j jobflavour]
"""
from __future__ import print_function

import os
import time
import datetime
import glob

available_flavours = [ 'espresso', 'microcentury', 'longlunch', 'workday', 'tomorrow', 'testmatch', 'nextweek' ]

def collect_rootfile_list(folder,pattern):
    """Creates an ordered list of root files found in a given folder.
    The list is written down into a file which could be used 
    in the `inputFiles_load` option of cmsRun.

    Parameters
    ----------
    folder: str
        The folder name where the root files are
    pattern: str
        Any pattern the root files should contain
    """
    if pattern is None:
        pattern = ''
    if pattern.find('*') == -1:
        pattern = '*'+pattern+'*'

    files = glob.glob('{}/{}.root'.format(folder,pattern))
    lines =[]
    for fn in sorted(files, key=lambda f: \
            int(f.split('.root')[0].split('_numEvent')[0].split('_')[-1])):
        lines.append('file:{}\n'.format(fn))
    
    fname='rootfiles_{}.list'.format(time.strftime('%Y%m%d-%H%M%S'))
    with open(fname,'w') as f:
        f.writelines(lines)
    print('Created root file list at "{}"'.format(fname))

def check_status(jobfolder,rsub_id,recreation):
    """Check the validity of the create output files

    Parameters
    ----------
    jobfolder: str
        The folder where the job where sent
    rsub_id: int|None
        The re-submittion id needed if the user want to
        check jobs which were re-submitted.        
    recreation: bool
        The default value is False. When used it becomes True
        and it calls another function that generates the file
        for resubmission.

    Raises
    ------
    IOError
        Whenever no *.err files are found inside an output/ folder
    """
    # Line indicating everything run fine:
    RUNFINE='dropped waiting message count 0'

    if rsub_id is None:
        # Naming: JOBNAME.JOBID.TASKID.err
        rsub_str = '*.*[0-9].*[0-9].err'
        rs_msg = ' '
    else:
        # Naming: JOBNAME.JOBID_ITERID.TASKID.err
        rsub_str = '*.*[0-9]_'+str(rsub_id)+'.*[0-9].err'
        rs_msg = ' for re-submission {} '.format(rsub_id)
    problems_outputs=[]
    good_outputs= []
    log_files = glob.glob('{}/output/{}'.format(jobfolder,rsub_str))
    if len(log_files) == 0:
        raise IOError("No log-files found{}at '{}/output'".format(rs_msg,jobfolder))

    for fname in sorted(log_files):
        jobid = '.'.join(os.path.basename(fname).split('.')[-3:-1])
        #print("Checking {0}".format(os.path.basename(fname)))
        with open(fname) as f:
            l = f.readlines()
        if len(l) == 0 or l[-1].strip('\n') != RUNFINE:
            problems_outputs.append('.'.join(os.path.basename(fname).split('.')[-3:-1]))
            #print("\033[1;33mFound some issue at\033[1;m {0}".format(jobid))
        else:
            good_outputs.append(jobid)

    print("\033[1;34m       Total  jobs:\033[1;m {0}".format(len(problems_outputs)+len(good_outputs)))
    print("----------------------------------")
    print("\033[1;32mGood Outputs  jobs:\033[1;m {0}".format(len(good_outputs)))
    if len(problems_outputs) != 0:
        plist = ','.join(sorted(problems_outputs,key=lambda x: int(x.split('.')[-1])))
        print("\033[1;33mProblematic   jobs:\033[1;m {0} [{1}]".format(len(problems_outputs),plist))

        if recreation:
            recreate_sub(plist)

def create_sub(scriptname,n_jobs,job_flavour):
    """Creates the sub file for the cluster job

    Parameter
    ---------
    scriptname: str
        The name of the job
    n_jobs: ing
        The number of jobs to launch
    job_flavour: str
        The flavour of the cluster
    """
    subfile ="executable   = {0}_$(ProcId).sh\n".format(scriptname)
    subfile+="arguments    = $(ClusterId) $(ProcId)\n"
    subfile+="output       = output/{0}.$(ClusterId).$(ProcId).out\n".format(scriptname)
    subfile+="error        = output/{0}.$(ClusterId).$(ProcId).err\n".format(scriptname)
    subfile+="log          = log/{0}.$(ClusterId).$(ProcId).log\n".format(scriptname)
    subfile+="+JobFlavour   = \"{0}\"\n".format(job_flavour)
    subfile+="queue {}\n".format(n_jobs)
    
    fname = scriptname+".sub"
    with open(fname,"w") as f:
        f.write(subfile)
        f.close()
    return fname

def recreate_sub (jobs_list):
    """Recreates the sub file for the cluster job when some jobs failed

    Parameter
    ---------
    jobs_list: str
        The list of jobs to be resubmitted
    """
    dirpath = os.getcwd()
    scriptname = os.path.basename(dirpath)
    script = scriptname + ".sub"

    with open(script, "r") as s:
        textfile = s.readlines()
        for line in textfile:
            if "+JobFlavour" in line:
                job_flavour = line.split('"')[1]

    failed_jobs_id = ''
    cluster_id = (jobs_list.split(',')[0]).split('.')[0]
    new_list = jobs_list.split(',')
    final_list = []

    for i in range(len(new_list)):
        final_list.append(new_list[i].split('.')[-1])

    for i in range(len(final_list)):
        if i == 0:
            failed_jobs_id = failed_jobs_id + final_list[i]
        else:
            failed_jobs_id = failed_jobs_id + " " + final_list[i]

    failed_jobs = failed_jobs_id.replace(" ", "\n ")

    subfile ="executable   = {0}_$(oldId).sh\n".format(scriptname)
    subfile+="arguments    = {0}_1 $(oldId)\n".format(cluster_id)
    subfile+="output       = output/{0}.{1}_1.$(oldId).out\n".format(scriptname, cluster_id)
    subfile+="error        = output/{0}.{1}_1.$(oldId).err\n".format(scriptname, cluster_id)
    subfile+="log          = log/{0}.{1}_1.$(oldId).log\n".format(scriptname, cluster_id)
    subfile+="+JobFlavour   = \"{0}\"\n".format(job_flavour)
    subfile+="queue oldId from (\n {0}\n)".format(failed_jobs)

    fname = scriptname+"_2resub.sub"
    
    with open(fname,"w") as f:
        f.write(subfile)
        f.close()

def create_bash(i,events,skip_events,jobname,pycfg,outputdir,inputfiles):
    """Creates the bash for the i-job. The sub-file (see `create_sub`) must 
    contain in the arguments: $(ClusterId) $(ProcId)

    Parameters
    ----------
    i: int
        The index of the current job
    events: int
        The events to be processed
    skip_events: int
        The events to skip before start to process
    jobname: str
        The name of the job, the bash scripts are name from it
    pycfg: str
        The configuration python file for the cmsRun. The file
        must contain the import of the `VarParsing` module from CMSSW.
        As example
        >>> import FWCore.ParameterSet.VarParsing as VarParsing
        >>> options = VarParsing.VarParsing ("analysis")
        >>> options.register("skipEvents",default=0)
        >>> options.maxEvents = -1
        >>> options.skipEvents = 0

        >>> options.parseArguments()

    outputdir: str
        The folder where to send the output root files
    inputfiles: str|None
        The list of input files, if needed (all jobs except GEN), or 
        a unique file containing the list of needed files, each one per row
    """
    ts = time.time()
    timestamp = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')

    # Check there is a CMSSW environment now
    if os.getenv('CMSSW_BASE') is None:
        raise RuntimeError("Please set the CMSSW environment first!")

    # If the input files are present, prepare the option to be included into 
    # cmsRun
    inputfiles_str=''
    cp_inputfiles_str =''
    if inputfiles is not None:
        # Add the logic to control if the files are root files, then
        # it should use inputFiles.
        # Otherwise, is a file which contains the list of the root files
        # and it should be using inputFiles_load
        if inputfiles.find('.root') != -1:
            inputfiles_str='inputFiles={}'.format(inputfiles)
        else:
            inputfiles_str='inputFiles_load={}'.format(inputfiles)
            cp_inputfiles_str = 'cp {0}/{1} .\n'.format(os.getcwd(),os.path.basename(inputfiles))

    bashfile = '#!/bin/bash\n\n'
    bashfile += '# File created by {0} [{1}]\n\n'.format(os.path.basename(__file__).replace('.py',''),timestamp)
    bashfile += 'cd {0}/src\n'.format(os.getenv('CMSSW_BASE'))
    bashfile += 'eval `scram runtime -sh`\n'
    bashfile += 'cd -\n'
    # Create a guard against malformed Workers (those which uses the same $HOME)
    bashfile += 'tmpdir=`mktemp -d`\ncd $tmpdir;\n\n'
    bashfile +='cp {0}/{1} .\n'.format(os.getcwd(),os.path.basename(pycfg))
    bashfile += cp_inputfiles_str
    bashfile +='cmsRun {0} outputFile={1}/{2}_{3}_{4}.root maxEvents={5} skipEvents={6} firstEvent={7} {8}; \n\n'.format(\
            os.path.basename(pycfg),
            outputdir,
            jobname,
            '${1}',
            '${2}',
            events,
            skip_events,
            skip_events+1,
            inputfiles_str)
    #bashfile +='cmsRun {0};\n'.format(self.py_cfg) outputFiles or algo?
    # remove the tmpdir
    bashfile +="rm -rf $tmpdir\n"
    fname = jobname+'_{0}.sh'.format(i)
    with open(fname,"w") as f:
        f.write(bashfile)
        f.close()
    os.chmod(fname,0755)

def main_create(jobname,job_flavour,pycfg,outputdir,njobs,max_events,inputfiles):
    """Create the CMSSW jobs for the HT-condor scheduler cluster

    Parameters
    ----------
    jobname: str
        The name of the job, to be used in the scripts as well
    job_flavour: str
        The flavour name of the cluster
    pycfg: str
        The configuration python file for the cmsRun. The file
        must contain the import of the `VarParsing` module from CMSSW.
        As example
        >>> import FWCore.ParameterSet.VarParsing as VarParsing
        >>> options = VarParsing.VarParsing ("analysis")
        >>> options.register("skipEvents",default=0)
        >>> options.maxEvents = -1
        >>> options.skipEvents = 0

        >>> options.parseArguments()

    outputdir: str
        The folder where to send the output root files
    njobs: int
        The number of jobs to be created
    max_events: int
        The maximum number of events to be processed. This define
        the number of events per job, but is the user who should enter
        it.
    inputfiles: str|None
        The list of input files, if needed (all jobs except GEN), or 
        a unique file containing the list of needed files, each one per row
    """
    evts_per_job=max_events/njobs
    remainder = (max_events % njobs)-1
    
    event_list = []
    for i in range(njobs-1):
        event_list.append( (i*evts_per_job,evts_per_job) )
    # The remaining
    event_list.append( ((njobs-1)*evts_per_job,remainder) )
    
    print("Creating [{0}] bash scripts: {1}_*.sh".format(len(event_list),jobname))
    for (i,(skipevents,evts)) in enumerate(event_list):
        create_bash(i,evts,skipevents,jobname,pycfg,outputdir,inputfiles)
    
    subfname = create_sub(jobname,njobs,job_flavour)
    print("Create the sub file for 'condor_submit': {0}".format(subfname))    
    print("Creating auxiliary folders...")
    try:
        os.mkdir('output')
    except OSError:
        pass
    
    try:
        os.mkdir('log')
    except OSError:
        pass

if __name__ == '__main__':
    from argparse import ArgumentParser,Action
    
    # Helper class to allow list the available flavours 
    class AvailFlavourAction(Action):
        def __init__(self,option_strings,dest,default=False,required=False,help=None):
            super(AvailFlavourAction, self).__init__(
                    option_strings=option_strings,
                    dest=dest,
                    nargs=0,
                    const=True,
                    default=default,
                    required=required,
                    help=help)

        def __call__(self, parser, namespace, values, option_string=None):
            print("\033[1;34mAvailable flavours:\033[1;m")
            for flavour in available_flavours:
                print(" - {0}".format(flavour))
            parser.exit()
    
    mesdsc="Create and check HT-condor cluster jobs for CMSSW"
    parser = ArgumentParser(prog='htcondor-jobs',description=mesdsc)
    
    # Sub-command parsers
    subparsers = parser.add_subparsers(title='subcommands',
            description='valid subcommands', 
            help='additional help')
    
    usage_c="Create HT-condor jobs"
    c_parser = subparsers.add_parser("create",description=usage_c)
    c_parser.add_argument('pycfg',help="The configuration python file for cmsRun")
    c_parser.add_argument('jobname',help="The name of the job, and related scripts")
    c_parser.add_argument('-o','--output-folder',action='store',dest='outputdir',\
            help="The folder where the output root files are sent")
    c_parser.add_argument('-i','--input-files',action='store',dest='inputfiles',\
            help="The list of input files, which could be a comma-separated list or "\
            "a file containing the actual files, each one per row")
    c_parser.add_argument('-n','--n-jobs',action='store',dest='njobs',type=int,\
            default=100,
            help="The number of jobs [Default: 100]")
    c_parser.add_argument('-f','--flavour',action='store',dest='job_flavour',\
            default='longlunch',
            help="The flavour of the cluster to be used with the jobs [Default: longlunch]")
    c_parser.add_argument('--available-flavours',action=AvailFlavourAction,\
            dest='available_flavours',
            help="The available flavour list")
    c_parser.add_argument('-m','--max-events',action='store',dest='max_events',type=int,\
            default=100000,
            help="The number of events to process [Default: 100000]")
    c_parser.set_defaults(which='create')
    
    usage_s="Check the output of the jobs once they finished"
    s_parser = subparsers.add_parser("status",description=usage_s)
    s_parser.add_argument('jobs_folder',help="The folder where the jobs where created")
    s_parser.add_argument('-r',action='store',type=int,dest='rid',\
            default=None,
            help="The resubmit id to check against (see re-submit)")
    s_parser.add_argument('--recreation',action='store_true',dest='recreation',help="Creation of the file to resubmit failed jobs")
    s_parser.set_defaults(which='status')
    
    usage_ct="Collect all the root files and write them down ordered in a file,"\
            " suitable to be used with the `inputFiles_load` cmsRun option."\
            " The command should be launched in the same directory intended to launch"\
            " the cmsRun command afterwards."
    ct_parser = subparsers.add_parser("collect",description=usage_ct)
    ct_parser.add_argument('roots_folder',help="The folder where are the root files")
    ct_parser.add_argument('-p','--pattern',action='store',dest='file_pattern',\
            help="Any pattern of the root files to be considered")
    ct_parser.set_defaults(which='collect')
    
    args = parser.parse_args()
    if args.which == 'create':
        main_create(args.jobname,
            args.job_flavour,
            args.pycfg,
            args.outputdir,
            args.njobs,
            args.max_events,
            args.inputfiles)
    elif args.which == 'status':
        check_status(args.jobs_folder,args.rid,args.recreation)
    elif args.which == 'collect':
        collect_rootfile_list(args.roots_folder,args.file_pattern)


