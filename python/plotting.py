#!/usr/bin/env python
"""Module with functions related with plotting at ROOT
"""
__author__ = "Jordi Duarte-Campderros"
__credits__ = ["Jordi Duarte-Campderros"]
__version__ = "0.1"
__maintainer__ = "Jordi Duarte-Campderros"
__email__ = "jorge.duarte.campderros@cern.ch"
__status__ = "Development"

__all__ = [ "set_sifca_style","setpalette","set_external_title"]

from array import array

import sys
# Trying to avoid the print out of the help from ROOT whenever a script
# uses this module. 
# Not sure  yet why is launching it... Looks like the SifcaStyle class
# object is created, and then is used ROOt.TStyle to obtain the SifcaStyle
# object class... 
if '-h' in sys.argv or \
        '--h' in sys.argv or\
        '--help' in sys.argv or \
        '-help' in sys.argv:
    class dummy(object):
        pass
    ROOT = dummy()
    ROOT.TStyle = object
else:
    import ROOT

    
class SifcaStyle( ROOT.TStyle ):
    """Implementation for a common SIFCA plot style. 
    Based in the class defined at https://github.com/nickcedwards/python-utils

    The Style should be set up by using the function
    ```set_sifca_style```
    """
    def __init__(self,name="SifcaStyle",title="SIFCA style object"):
        """
        The constructor initialize the TStyle object and configures it
        to set up the Style 

        Parameters
        ----------
        name: str, default ["SifcaStyle"]
            The internal name of the style
        title: str, default ["SIFCA style object"]
            The title of the style
        """
        ROOT.TStyle.__init__(self,name,title)
        self.SetName(name)
        self.SetTitle(name)

        # Set up the style
        self.configure()

        return
    
    def configure(self):
        """Definition of the style
        """
        self.Info("configure","Configuring SIFCA style")

        #--------------------------------------------------------------------------
        # Legend
        #--------------------------------------------------------------------------
        self.SetTextFont(132)
        self.SetTextSize(0.045)
        self.SetLegendBorderSize(0)
        self.SetLegendFillColor(0)

        
        #--------------------------------------------------------------------------
        # Canvas
        #--------------------------------------------------------------------------
        self.SetCanvasBorderMode(  0)
        self.SetCanvasBorderSize( 10)
        self.SetCanvasColor     (  0)
        self.SetCanvasDefH(550)
        self.SetCanvasDefW(700)
        self.SetCanvasDefX(0)
        self.SetCanvasDefY(0)
        #
        ##--------------------------------------------------------------------------
        ## Pad
        ##--------------------------------------------------------------------------
        self.SetPadBorderMode  (   0)
        self.SetPadBorderSize  (  10)
        self.SetPadColor       (   0)
        self.SetPadBottomMargin(0.14)
        # XXX Note in order to release some empty space to write down
        # legends or other useful info, a large area is needed SetTopMargin(0.25)?
        self.SetPadTopMargin   (0.08)
        self.SetPadLeftMargin  (0.14)
        self.SetPadRightMargin (0.08)
        
        #--------------------------------------------------------------------------
        # Frame
        #--------------------------------------------------------------------------
        self.SetFrameFillStyle ( 0)
        self.SetFrameFillColor ( 0)
        self.SetFrameLineColor ( 1)
        self.SetFrameLineStyle ( 0)
        self.SetFrameLineWidth ( 2)
        self.SetFrameBorderMode( 0)
        self.SetFrameBorderSize(10)
        
        #--------------------------------------------------------------------------
        # Hist
        #--------------------------------------------------------------------------
        self.SetHistFillColor(0)
        self.SetHistFillStyle(1)
        self.SetHistLineColor(1)
        self.SetHistLineStyle(0)
        self.SetHistLineWidth(2)
        
        #--------------------------------------------------------------------------
        # Func
        #--------------------------------------------------------------------------
        self.SetFuncWidth(3)
        self.SetFuncColor(ROOT.kRed+1)
        
        #--------------------------------------------------------------------------
        # Title
        #--------------------------------------------------------------------------
        self.SetTitleBorderSize(    0)
        self.SetTitleFillColor (    0)
        self.SetTitleX         (0.5)
        self.SetTitleAlign     (   23)
        self.SetTitleFont(132)
        self.SetTitleSize(0.045)
        
        #--------------------------------------------------------------------------
        # Stat
        #--------------------------------------------------------------------------
        self.SetStatBorderSize(0)
        self.SetStatColor(0)
        
        #--------------------------------------------------------------------------
        # Axis
        #--------------------------------------------------------------------------
        #self.SetPadTickX(1)  # Tick marks on the opposite side of the frame
        #self.SetPadTickY(1)  # Tick marks on the opposite side of the frame
        self.SetTitleFont(132, "X")
        self.SetTitleFont(132, "Y")
        self.SetTitleFont(132, "Z")
        self.SetTitleSize(0.045,"X")
        self.SetTitleSize(0.045,"Y")
        self.SetTitleSize(0.045,"Z")

        self.SetTitleOffset(1.4,"x")
        self.SetTitleOffset(1.2,"y")
        self.SetTitleOffset(1.2,"z")

        self.SetLabelFont(132, "X")
        self.SetLabelFont(132, "Y")
        self.SetLabelFont(132, "Z")
        self.SetLabelSize(0.045,"X")
        self.SetLabelSize(0.045,"Y")
        self.SetLabelSize(0.045,"Z")

        # ---------------------------------------
        # Extra
        # ---------------------------------------    
        self.SetNumberContours(99)
        self.SetMarkerStyle(20)
        self.SetMarkerSize(0.7)

        # Fix a problem with the palette: it appears to be uncentered,
        # Needs to plot any COLZ histo first with COL, and
        # afterwards using the COLZ
        self.SetPalette(ROOT.kBird)
        
        return self


def set_sifca_style(squared=False,ratio_1to4=False,ratio_1to2=False,stat_off=False):
    """Return a ROOT.gStyle to be used for the SIFCA group.
    The function returns a SifcaStyle instance

    Example
    -------
    In order to force to the ROOT.TObject 'h' to use the style
    >>> h.UseCurrentStyle()

    Parameters
    ----------
    squared: bool, optional
        Whether or not the style will assume square plots,
        therefore setting up the proper layout
    ratio_1to4: bool, optional
        Whether or not the style will assume rectangular plots,
        in proportion 1 (y-axis) to 4 (x-axis). This proportion
        is used for 2x2 or 4x4 sensor cells on 25x100 pitch sensors
    ratio_1to2: bool, optional
        Whether or not the style will assume rectangular plots,
        in proportion 1 (y-axis) to 2 (x-axis). This proportion
        is used for to show the real sensor
    stat_off: bool, optional
        Whether or not allow the statistical box appear

    Return
    ------
    ROOT.TStyle instance
    """
    style = SifcaStyle()
    ROOT.gROOT.SetStyle(style.GetName())
    # Force square canvases
    if squared:
        style.SetCanvasDefH(900)
        style.SetCanvasDefW(724)
        style.SetPadTopMargin(0.30)
        style.SetPadRightMargin (0.18)
        style.SetTitleOffset(1.4,"x")
        style.SetTitleOffset(1.6,"y")
        style.SetTitleOffset(1.5,"z")
        style.SetNdivisions(506, "X");
        style.SetNdivisions(506, "Y");
        style.SetTitleX     (0.56)
    elif ratio_1to4:
        # Force rectangular canvases
        style.SetCanvasDefH(600)
        style.SetCanvasDefW(1000)
        style.SetPadRightMargin(0.15)
        style.SetPadLeftMargin(0.12)
        style.SetPadTopMargin(0.53)
        style.SetNdivisions(205, "Y");
        style.SetNdivisions(5, "Z");
        style.SetTitleOffset(1.4,"X")
        style.SetTitleOffset(1.2,"Y")
        style.SetTitleOffset(1.0,"Z")
        style.SetTitleX     (0.56)
    elif ratio_1to2:
        # Force rectangular canvases
        style.SetCanvasDefH(600)
        style.SetCanvasDefW(1000)
        style.SetPadRightMargin(0.15)
        style.SetPadLeftMargin(0.12)
        style.SetPadTopMargin(0.30)
        style.SetNdivisions(205, "Y");
        style.SetNdivisions(5, "Z");
        style.SetTitleOffset(1.4,"X")
        style.SetTitleOffset(1.2,"Y")
        style.SetTitleOffset(1.0,"Z")
        style.SetTitleX     (0.56)

    # Force not to show the Opt canvas
    if stat_off:
        style.SetOptStat(0)
    # Force style to be applied to plots made under different
    # style
    ROOT.gROOT.ForceStyle()
    # Maximum number of digits in the axis
    ROOT.TGaxis.SetMaxDigits(4)
    return style

def prepare_TH2D_at_1to4_plot(h):
    """Function to be called just before the Draw of a TH2 with 
    the COLZ option in the ratio_1to4 layout (see set_sifca_style
    function). This function corrects the wide on the Z column.
    
    Parameters
    ----------
    h: ROOT.TH2
        The histogram to be plotted

    Return
    ------
    ROOT.TCanvas

    Raise
    -----
    IndexError: when the histogram is empty
    """
    c = ROOT.TCanvas()
    h.Draw("COLZ")
    c.Update()
    pl=h.GetListOfFunctions()[0]
    # Slim the z-palette
    pl.SetX2(pl.GetX1()+(pl.GetX2()-pl.GetX1())*0.5)
    c.Update()
    return c

def setpalette(name="rainbow", ncontours=99):
    """Set a color palette from a given RGB list
    stops, red, green and blue should all be lists 
    of the same length  
    """
    if name == "gray" or name == "grayscale":
        stops = [0.00, 0.34, 0.61, 0.84, 1.00]
        red   = [1.00, 0.84, 0.61, 0.34, 0.00]
        green = [1.00, 0.84, 0.61, 0.34, 0.00]
        blue  = [1.00, 0.84, 0.61, 0.34, 0.00]
    elif name == 'darkbody':
        stops = [0.00, 0.25, 0.50, 0.75, 1.00]
        red   = [0.00, 0.50, 1.00, 1.00, 1.00]
        green = [0.00, 0.00, 0.55, 1.00, 1.00]
        blue  = [0.00, 0.00, 0.00, 0.00, 1.00]
    elif name == 'inv_darkbody':
        stops = [0.00, 0.25, 0.50, 0.75, 1.00] 
        red   = [1.00, 1.00, 1.00, 0.50, 0.00]
        green = [1.00, 1.00, 0.55, 0.00, 0.00]
        blue  = [1.00, 0.00, 0.00, 0.00, 0.00]
    elif name == 'deepsea':
        stops = [0.00, 0.34, 0.61, 0.84, 1.00]  
        red   = [0.00, 0.09, 0.18, 0.09, 0.00]
        green = [0.01, 0.02, 0.39, 0.68, 0.97] 
        blue  = [0.17, 0.39, 0.62, 0.79, 0.97] 
    elif name == 'forest':
        stops = [0.00, 0.25, 0.50, 0.75, 1.00]  
        red   = [0.93, 0.70, 0.40, 0.17, 0.00]
        green = [0.97, 0.89, 0.76, 0.64, 0.43] 
        blue  = [0.98, 0.89, 0.64, 0.37, 0.17] 
    else:
        # default palette, looks cool
        stops = [0.00, 0.34, 0.61, 0.84, 1.00]
        red   = [0.00, 0.00, 0.87, 1.00, 0.51]
        green = [0.00, 0.81, 1.00, 0.20, 0.00]
        blue  = [0.51, 1.00, 0.12, 0.00, 0.00]

        
    s = array('d', stops)
    r = array('d', red)
    g = array('d', green)
    b = array('d', blue)
    
    npoints = len(s)
    ROOT.TColor.CreateGradientColorTable(npoints, s, r, g, b, ncontours)
    ROOT.gStyle.SetNumberContours(ncontours)

def set_external_title(c,first_cms_line=False,**kwd):
    """Place the a title with multiple lines in the position 

    Example
    -------
    To right a title with 3 lines and the text: 

    Hi there,
    this is a 
    test

    the function should be called as:
    >>> set_external_title(line1="Hi there,",line2='this is a',line3='test')
    
    Parameters
    ----------
    c: ROOT.TCanvas
    first_cms_line: bool
        Whether or not the first line introduced in kwd, i.e., line0,
        should be considered type "CMS". This line will be treated differently
        than the others (text size and bold)
    kwd: Should contain the list of text to be place, where the keyword
         arguments must follow the convection 'line#'

    Return
    ------
    ROOT.TCanvas
    """
    assert len(kwd) > 0,"Call to 'set_external_title' with empty lines to be filled"
    # Get largest string
    x2=0.03*max(map(lambda x: len(x),kwd.values()))
    # Get the number of lines
    #y2=(1.0-c.GetTopMargin())+0.05+0.065*len(kwd)
    # Get the position of the first line
    #y1=(1.0-c.GetTopMargin())+0.065*len(kwd)
    y1 = 1.0-(c.GetTopMargin()-0.055*len(kwd))
    # Get the position of the left axis:
    x1 = c.GetLeftMargin()
    # XXX - TBD
    #y1=y2-0.065*len(kwd)
    #pt=ROOT.TPaveText(0.02,y1,x2,y2,"NDC")
    pt = ROOT.TLatex()
    pt.SetNDC()
    pt.SetTextAngle(0)
    pt.SetTextColor(ROOT.kBlack)

    pt.SetTextAlign(11)
    text_font = 132
    original_text_size = pt.GetTextSize()
    text_size = pt.GetTextSize()
    if first_cms_line:
        text_font = 61
        text_size *= 1.1
    for i,key in enumerate(sorted(kwd,key=lambda x: int(x.replace('line','')))):
        pt.SetTextFont(text_font)
        pt.SetTextSize(text_size)
        #pt.AddText(kwd[key])
        pt.DrawLatex(x1,y1-0.045*i,kwd[key])
        text_font = 132
        text_size = original_text_size
    pt.Draw()
    return c,pt


def set_attr_plotobject(gr,**kwd):
    """Set some attributes to a ROOT.THX or ROOT.TGraphXX
    objects

    [Original function extracted from 
    https://github.com/duartej/PyAnUtils/python/pyanfunctions.py]

    Parameters
    ----------
    gr: ROOT.TObject (should be TGraph or THX)
        The object to set the attributes
    color: int, optional [Default: a random color is provided]
    linestyle: int, optional [Default: 1]
    markerstyle: int, optional [Default: 20]
    linewidth: int, optional [Default: 2]
    """
    opt = ExtraOpt( [ ('color',None), ('linestyle',1),
        ('markerstyle',20), ('linewidth',2),
        ('markersize',0.7),
        ('title', ''), 
        ('xtitle', None), ('ytitle',None), ('ztitle',None)] )
    opt.setkwd(kwd)

    if not opt.color:
        import random
        color = int(random.uniform(1,1000))
    else:
        color = opt.color
    
    gr.SetMarkerStyle(opt.markerstyle)
    gr.SetMarkerSize(opt.markersize)
    gr.SetMarkerColor(color)

    gr.SetLineStyle(opt.linestyle)
    gr.SetLineColor(color)
    gr.SetLineWidth(opt.linewidth)

    # Titles 
    gr.SetTitle(opt.title)
    titlesandacces = map(lambda x: (x+'title','Get'+x.upper()+'axis'), \
            [ 'x', 'y', 'z' ])
    for (title,method) in titlesandacces:
        # if defined (i.e. not None)
        if getattr(opt,title):
            getattr(gr,method)().SetTitle(getattr(opt,title))

def get_tripads_frame(c):
    """Prepare the canvas with threeTPads suitable to create inside
    a a 2-dim plots sourrended by its profile or projections.
    In order to use it, don't forget to use TPad.cd to the 
    targeted pad before draw
      
       ___   ________________  
      | p | |                | 
      | a | |                | 
      | d | |     padup      | 
      | l | |                | 
      |___| |________________| 
             ________________
            |    paddown     |
            |________________|

    
    Parameters
    ----------
    c: ROOT.TCanvas()
        the canvas where the pads are included

    Return:
    padup: ROOT.TPad
        the upper pad
    paddown: ROOT.TPad
        the downer pad
    padleft: ROOT.TPad
        the lef pad
    """
    # The pad to place the main plot
    padup = ROOT.TPad("padup_{0}".format(hash(c)),"padup",0.26,0.3,1,1)
    padup.Draw()
    padup.cd()
    padup_height = padup.GetTopMargin()-padup.GetBottomMargin()
    # the pad to place the down plot (x-profile/projection)
    c.cd()
    paddown = ROOT.TPad("paddown_{0}".format(hash(c)),"paddown",0.26,0.03,1,0.29)
    # Let's extract the size relatively to the main pad: 1/3 height of the main
    # --------------------------------------------------------------------------
    paddown_height = 1./3.*padup_height
    paddown.SetBottomMargin((1.-paddown_height)/8.0)
    paddown.SetTopMargin((1.-paddown_height)/8.0)
    paddown.Draw()
    paddown.cd()
    c.cd()
    padleft = ROOT.TPad("padleft_{0}".format(hash(c)),"padleft",0,0.3,0.25,1)
    # Same as down
    padleft_height=paddown_height
    #padleft.SetLeftMargin(0.02)
    # be careful with the rotation
    padleft.SetRightMargin(padup.GetTopMargin()) 
    padleft.SetLeftMargin(padup.GetBottomMargin()) 
    padleft.SetTopMargin((1.-padleft_height)/4.0)
    padleft.SetBottomMargin((1.-padleft_height)/4.0)
    padleft.Draw()
    padleft.cd()
    c.cd()
    return padup,paddown,padleft

def create_tripad_plot(href,is_profile=True):
    """Prepare the canvas with three TPads and create inside
    a 2-dim map plot in the main pad, and its X (bottom pad)
    and y-profile (left pad)  or projectionms

    href: ROOT.TH2F
        the histogram which will play the role of data (numerator in 
        the ratio plot)
    is_profile: bool
        Whether or not to use a profile or a projection

    Return
    ------
    c, __container: (TCanvas, (TPad,TPad,TPad,TProfile,TProfile))
        note that the `__container` n-tuple is just returned to avoid 
        the objects destruction once they go out of scope

    Notes
    -----
    If this function is called multiples times, the returned canvas should
    be deleted whenever it is not needed anymore. Otherwise, a lot of canvases
    can cause a segmentation fault (machine dependent), because ROOT does not
    free the canvases until the application totally finish. To delete the 
    canvases, just use
        ROOT.gROOT.GetListOfCanvases().Delete()
    """
    if not is_profile:
        hpx=href.ProjectionX
        hpy=href.ProjectionY
    else:
        hpx= href.ProfileX
        hpy= href.ProfileY

    # -- Create the Canvas and pads
    c = ROOT.TCanvas()
    pu,pd,pl= get_tripads_frame(c)

    # -- Regular plots with the histos in the upper pad
    # ---- need to get the maximum y first
    pu.cd()
    href.Draw("COLZ")

    c.cd()
    # Extract profile or projection?
    # -- Draw the down profile
    pd.cd()
    xprof=hpx('{}_pfx_{}'.format(href.GetName(),hash(href)))
    # Some attributes: remove x-axis, enlarge y-axis 
    xprof.GetXaxis().SetLabelSize(0)
    xprof.GetYaxis().SetTitleSize(0.15)
    xprof.GetYaxis().SetLabelSize(0.14)
    xprof.Draw('X+')
    
    ti = ROOT.TASImage.Create()
    pl.cd()
    yprof=hpy('{}_pfy_{}'.format(href.GetName(),hash(href)))
    # Some attributes: remove x-axis, enlarge y-axis 
    yprof.GetXaxis().SetLabelSize(0)
    yprof.GetYaxis().SetTitleSize(0.06)
    yprof.GetYaxis().SetLabelSize(0.05)
    yprof.Draw()
    # Rotate the plot 
    ti.FromPad(pl)
    ti.Flip(90)
    pl.Draw()
    pl.cd()
    ti.Draw('x')
    c.Update()
    
    # just keeping the created objects to avoid destruction when
    # going out of scope
    # XXX: Maybe you can use the ROOT.gDirectory.Add method
    __container = (pu,pd,pl,xprof,yprof)

    return c,__container

