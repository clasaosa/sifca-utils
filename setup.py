import setuptools

setuptools.setup(
        name='sifca-utils',
        version='0.1',
        package_dir={'sifca_utils':'python'},
        packages = ['sifca_utils'],
        #packages=find_packages(),
        scripts=['bin/htcondor_jobs'],

        #install_requires=["pkgname>=v.v"],
        
        # Package data 
        #package_data={},

        # Metadata to display on PyPI
        author='Jordi Duarte-Campderros',
        author_email='jorge.duarte.campderros@cern.ch',
        description='SIFCA analysis utils',
        url='https://gitlab.cern.ch/sifca/sifca-utils',
        # See https://docs.python.org/2/distutils/setupscript.html#listing-whole-packages
        # for changes in the package distribution
        classifiers=[
            "Programming Language :: Python :: 2.7",
            "License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)",
            "Operating System :: OS Independent",
            "Development Status :: 4 - Beta",
            ],
        )
